//
//  ViewController.swift
//  swift_model_example
//
//  Created by renpeng on 10/9/20.
//

import UIKit
import Charts

class ViewController: UIViewController {
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var step_counter: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Start_process_pipline(self.chartView, self.step_counter)
    }
}

