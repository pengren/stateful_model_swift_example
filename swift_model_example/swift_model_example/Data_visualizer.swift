//
//  Data_visualizer.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/11/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import Charts

class Data_visualizer{
    func generate_lines(_ input_seq: [Double], _ label_name: String, _ line_color: [UIColor]) -> LineChartDataSet{
            var chartEntry = [ChartDataEntry]()
            for i in 0..<input_seq.count {
                chartEntry.append(ChartDataEntry(x: Double(i)/25.0, y: input_seq[i]))
            }
            let line = LineChartDataSet(entries: chartEntry, label: label_name)
            line.drawCirclesEnabled = false
            line.drawValuesEnabled = false
            line.colors = line_color
            return line
        }
        
    func updateChart(_ origin_result: [Double], _ filtered_result: [Double], _ azimuth_result: [Double], _ chartView: LineChartView) {
            // Use this func to update the chart in realtime
            let data = LineChartData()
        
            let origin_pred_line = self.generate_lines(origin_result, "thresholded result", [UIColor.red])
            let filtered_pred_line = self.generate_lines(filtered_result, "raw step counter output", [UIColor.purple])
            let azimuth_data_line = self.generate_lines(azimuth_result, "final count result", [UIColor.cyan])
            
            data.addDataSet(origin_pred_line)
            data.addDataSet(filtered_pred_line)
            data.addDataSet(azimuth_data_line)
            
            chartView.rightAxis.drawGridLinesEnabled = false
            chartView.rightAxis.drawLabelsEnabled = false
    //      chartView.setVisibleXRangeMaximum(2)
    //      chartView.dragXEnabled = true
    //      chartView.doubleTapToZoomEnabled = false
            chartView.leftAxis.axisMinimum = -0.25
            chartView.leftAxis.axisMaximum = 1.25
//            chartView.leftAxis.axisMinimum = -Double.pi
//            chartView.leftAxis.axisMaximum = Double.pi
            chartView.data = data
            chartView.chartDescription?.text = "Step counter Chart"
        }
}
