//
//  Step_counter_detector.swift
//  CoreMotionExample
//
//  Created by renpeng on 5/29/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import Firebase
class Step_counter_detector{
    // For RNN case
    let hidden_unit: Int
    var state_h_1: [Double]
    var state_c_1: [Double]
    var state_h_2: [Double]
    var state_c_2: [Double]
    
    var running_feature_array: [Double]
    
    // For debug purpose
    var prob_tracker: [Double]
    // For tensorflow lite case
    var model: CustomLocalModel!
    var interpreter: ModelInterpreter!
    var ioOptions: ModelInputOutputOptions!
    
    // Moving avg & std
    var MSA : moving_std_avg!
    

    init(hidden_unit: Int) {
        self.hidden_unit = hidden_unit
        // Saver for previous hidden states, initialized with zeros
        self.state_h_1 = Array(repeating: Double(0.0), count: self.hidden_unit)
        self.state_c_1 = Array(repeating: Double(0.0), count: self.hidden_unit)
        self.state_h_2 = Array(repeating: Double(0.0), count: self.hidden_unit)
        self.state_c_2 = Array(repeating: Double(0.0), count: self.hidden_unit)
        
        self.MSA = moving_std_avg()
        
        self.running_feature_array = [Double]()
        
        self.prob_tracker = [Double]()
        // Load tensorflow lite model
        let is_load_model_success = self.loadModel()
        if is_load_model_success{
            print("Model loaded successfully!")
            self.buildInterpreter()
            print("interpreter built successfully!")
        }
    }
    
    private func loadModel() -> Bool {
        // Step 0. Note that this steps are coordinate with:  ✅ whole process of the ios check
        // Step 1. change model name: "pure_ae" -> "info_vae_encoder"
        guard let modelPath = Bundle.main.path(forResource: "step_counter_fine_tuned", ofType: "tflite")
            else {
                // Invalid model path
                print("invalid model path")
                return false
        }
        model = CustomLocalModel(
            modelPath: modelPath
        )
        return true
    }
    
    private func buildInterpreter() {
        let _interpreter = ModelInterpreter.modelInterpreter(localModel: model)
        
        let _ioOptions = ModelInputOutputOptions()
        do {
            // Step 2. Change input shape: [1, 64, 3] -> [1, 64, 2]
            try _ioOptions.setInputFormat(index: 0, type: .float32, dimensions: [1, 1, 6])
            
            try _ioOptions.setInputFormat(index: 1, type: .float32, dimensions: [1, 8])
            try _ioOptions.setInputFormat(index: 2, type: .float32, dimensions: [1, 8])
            try _ioOptions.setInputFormat(index: 3, type: .float32, dimensions: [1, 8])
            try _ioOptions.setInputFormat(index: 4, type: .float32, dimensions: [1, 8])
            // Step 3. Change output shape: [1, 64, 3] -> [1, 4]
            try _ioOptions.setOutputFormat(index: 0, type: .float32, dimensions: [1, 1])
            
            try _ioOptions.setOutputFormat(index: 1, type: .float32, dimensions: [1, 8])
            try _ioOptions.setOutputFormat(index: 2, type: .float32, dimensions: [1, 8])
            try _ioOptions.setOutputFormat(index: 3, type: .float32, dimensions: [1, 8])
            try _ioOptions.setOutputFormat(index: 4, type: .float32, dimensions: [1, 8])
            
        } catch let error as NSError {
            print("error setting up model io: \(error)")
        }
        // initialize members
        interpreter = _interpreter
        ioOptions = _ioOptions
    }
    
    
    public func process_input_feature_data(feature_data: [Double],
                                           Stop_interval_detect: Double,
                                           completion: @escaping (_ result: Double)->()){
        // Don't forget to normalize the feature_data!
        running_feature_array.append(contentsOf: feature_data)
//        let normalized_feature = feature_data.map { ($0 - mean) / stddev }
//        let normalized_feature = self.Data_normalization(input_data: feature_data)
        
        let normalized_feature = self.MSA.moving_array_update_and_scale(feature_data, Stop_interval_detect)
        
        // Using RNN to process input feature data so that we can get the step counts
        var inputs = ModelInputs()
        inputs = self.fill_in_data(feature_data: normalized_feature, inputs: inputs)
        self.fetch_outputs(inputs: inputs) { (result) -> () in
            // do stuff with the result
            // print(result)
            completion(result)
        }
        // let outputs = self.fetch_outputs(inputs: inputs)
        // return outputs
    }
    
    private func fill_in_data(feature_data: [Double], inputs: ModelInputs)->ModelInputs{
        var res_inputs = inputs
        // all types of features
        res_inputs = self.fill_in_single_seq_data(input_seq: feature_data, inputs: res_inputs)
        // State 1 -h
        res_inputs = self.fill_in_single_seq_data(input_seq: self.state_h_1, inputs: res_inputs)
        // State 1 -c
        res_inputs = self.fill_in_single_seq_data(input_seq: self.state_c_1, inputs: res_inputs)
        
        // State 2 -h
        res_inputs = self.fill_in_single_seq_data(input_seq: self.state_h_2, inputs: res_inputs)
        // State 2 -c
        res_inputs = self.fill_in_single_seq_data(input_seq: self.state_c_2, inputs: res_inputs)
        
//        print("feature_data: \(feature_data)")
//        print("state_h_1: \(self.state_h_1)")
//        print("state_c_1: \(self.state_c_1)")
//        print("state_h_2: \(self.state_h_2)")
//        print("state_c_2: \(self.state_c_2)")
        return res_inputs
    }
    
    private func fill_in_single_seq_data(input_seq: [Double], inputs: ModelInputs)->ModelInputs{
        // Given the single seq of data, input these data into the model
        var inputData = Data()
        do {
            for single_ele in input_seq{
                // Turn to Float32
                var single_ele = Float32(single_ele)
                let elementSize = MemoryLayout.size(ofValue: single_ele)
                var bytes = [UInt8](repeating: 0, count: elementSize)
                memcpy(&bytes, &single_ele, elementSize)
                inputData.append(&bytes, count: elementSize)
            }
            try inputs.addInput(inputData)
        }
        catch let error
        {
        print("Failed to add step input: \(error)")
        }
        return inputs
    }
    
    private func fetch_outputs(inputs: ModelInputs, completion: @escaping (_ result: Double)->()){
        interpreter.run(inputs: inputs, options: ioOptions) {
            outputs, error in
            guard error == nil, let outputs = outputs else {
                print("step interpreter error")
                if (error != nil) {
                    print(error!)
                }
                return
            }
            // Get first and only output of inference with a batch size of 1
            let model_res = try! outputs.output(index: 0)
            let model_target = model_res as! [[Double]]
            
            // Also, update the hidden states
            let output_state_1 = try! outputs.output(index: 1)
            let D2_output_1 = output_state_1 as! [[Double]]
            self.state_h_1 = D2_output_1[0]
            
            let output_state_2 = try! outputs.output(index: 2)
            let D2_output_2 = output_state_2 as! [[Double]]
            self.state_c_1 = D2_output_2[0]
            
            let output_state_3 = try! outputs.output(index: 3)
            let D2_output_3 = output_state_3 as! [[Double]]
            self.state_h_2 = D2_output_3[0]
            
            let output_state_4 = try! outputs.output(index: 4)
            let D2_output_4 = output_state_4 as! [[Double]]
            self.state_c_2 = D2_output_4[0]
            
            // Print out the states values: The states are correct...
            // If the input do not Spoil, then the model should be correct..
            // So so so, our model is good.. the main problem is about the cpu usage.. I suppose
//            print("h_1: \(self.state_h_1 )")
//            print("c_1: \(self.state_c_1 )")
//            print("h_2: \(self.state_h_2 )")
//            print("c_2: \(self.state_c_2 )")
            
            // Fetch final results
            let class_1_prob = model_target[0][0]
            self.prob_tracker.append(class_1_prob)
            
            completion(class_1_prob)
        }
    }
    
    public func debug_extract_class_1_prob_path()->[Double]{
        // return the class 1 prob
        return self.prob_tracker
    }
}
