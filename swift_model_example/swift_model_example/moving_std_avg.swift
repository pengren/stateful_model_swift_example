//
//  moving_std_avg.swift
//  CoreMotionExample
//
//  Created by renpeng on 6/12/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
public class moving_std_avg{
    var moving_mean_array: [Double]
    var moving_mean_square_array: [Double]
    var moving_std_array: [Double]
    var counter: Double
    
    init() {
        // Initialize mean array and std array
//        self.moving_mean_array =
//            [-0.00261932,  0.00181575, -0.00299743,
//             0.00900849, -0.01532054, 0.02732752]
        self.moving_mean_array =
//        [ 0.00044751, -0.00025806, -0.00560017,  0.00112017, -0.0020755 ,
//        0.01212669]
            [-7.67118413e-05, -2.04009131e-03, -5.60016768e-03, -1.39189680e-02,
            -5.32549138e-03,  1.21266930e-02]
        self.moving_mean_square_array = self.moving_mean_array.map{$0 * $0}
//        self.moving_std_array =
//            [0.35195228, 0.83323698, 0.18330981,
//             0.14313237, 0.14000793, 0.17354385]
        self.moving_std_array =
//        [0.6919833 , 0.6834478 , 0.77810737, 0.21891395, 0.22271902,
//        0.23144606]
            [0.68590156, 0.68954836, 0.77810737, 0.22056518, 0.22059363,
            0.23144606]
        
        // Note the initial value is utitlized here
        self.counter = 1.0
    }
    
    public func moving_array_update_and_scale(_ data: [Double], _ Stop_interval_detect: Double) -> [Double]{
        // input data has length 6. There are 6 features in total
        assert(data.count == 6)
        var copied_data = data
        
        // Add one to the counter
        self.counter += 1
        
        if Stop_interval_detect == 0.0{
            // Update mean array
            for i in 0..<6{
                self.moving_mean_array[i]
                    = ((self.counter - 1.0)*self.moving_mean_array[i] + data[i])/self.counter
                
                self.moving_mean_square_array[i]
                    = ((self.counter - 1.0)*self.moving_mean_square_array[i] +
                        data[i] * data[i])/self.counter
                
                self.moving_std_array[i]
                    = sqrt(
                        self.moving_mean_square_array[i] -
                            self.moving_mean_array[i] * self.moving_mean_array[i]
                    )
            }
//            print("updating")
        }
        else{
//            print("Not updating")
        }
        
        for i in 0..<6{
            // Use the updated data to scale the input data value
            copied_data[i] = (data[i] - self.moving_mean_array[i])/self.moving_std_array[i]
        }
        
//        print("moving mean array: \(self.moving_mean_array)")
//        print("moving std array: \(self.moving_std_array)")
//        print("scaled data: \(copied_data)")
        return copied_data
    }
    
//    public func test_case(){
//        self.moving_array_update_and_scale([0, 1, 2, 3, 4, 5])
//        self.moving_array_update_and_scale([5, 3, 1, 0, 9, 7])
//        self.moving_array_update_and_scale([1, 2, 6, 7, 3, 1])
//    }
}
