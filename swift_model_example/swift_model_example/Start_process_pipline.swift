//
//  Start_process_pipline.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/11/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import AVFoundation
import CoreMotion
import UIKit
import Charts

class Start_process_pipline{
    // Constants
    let window_size: Int // Size of input to the RNN during training process, note that we do not train the model here
    
    let view_length: Int // The length of the data to be visualized
    let step_counter_in_threshold: Double
    let step_counter_out_threshold: Double
    
    // Results recorder
    var filtered_result_recorder: [Double] // Array contains the turn interval detection results
    
    // Declare of different classes
    let motionManager: CMMotionManager!
    let sc_filter: step_counter_postprocess_filter!
    let chart_manager: Data_visualizer!
    let step_semaphore: DispatchSemaphore!
    let C_normlize: Coordinate_normalization!
    var Step_counter: Step_counter_detector!
    var chartView: LineChartView!
    var counter_label: UILabel!
    var timer: Timer!
    
    init(_ chartView: LineChartView, _ counter_label: UILabel) {
        // Constants
        self.window_size = 50
        self.view_length = 250
        self.step_counter_in_threshold = 0.55
        self.step_counter_out_threshold = 0.25
        
        // Results recorder
        self.filtered_result_recorder = [Double]()
        // Declare of different classes
        self.motionManager = CMMotionManager()
        self.sc_filter = step_counter_postprocess_filter(self.step_counter_in_threshold, self.step_counter_out_threshold)
        self.chart_manager = Data_visualizer()
        self.C_normlize = Coordinate_normalization()
        self.step_semaphore = DispatchSemaphore(value: 0)
        self.Step_counter = Step_counter_detector(hidden_unit: 8)
        self.chartView = chartView
        self.counter_label = counter_label
        self.timer = Timer.scheduledTimer(timeInterval: 1/25, target: self, selector: #selector(self.update), userInfo: nil, repeats: true) // The timeInterval here is 1/25, in other words, 25 HZ
        
        // Initialize sensors, choose which sensor to open
        self.Select_open_sensors()
    }
    
    func Select_open_sensors(){
        motionManager.startDeviceMotionUpdates()
    }
    
    @objc func update() {
        // This func run 25 times per second
        if let deviceMotion = motionManager.deviceMotion{
            let (_, rotationMatrix) = Data_Generation.attitude_gen(deviceMotion: deviceMotion)
            
            let normalized_input_features = self.C_normlize.process_data(rotationRate: deviceMotion.rotationRate, userAcceRate: deviceMotion.userAcceleration, rotationMatrix: rotationMatrix)
            // Delay removed here according to the instruction
            self.async_step_detector(normalized_input_features)
            self.update_chart()
            
            DispatchQueue.main.async {
                self.counter_label.text = "Counter: \(self.sc_filter.counter)"
            }
        }
    }
    
    func async_step_detector(_ feature: [Double]){
        // Perform turn detector based on features
        DispatchQueue.global().async {
            self.Step_counter.process_input_feature_data(feature_data: feature, Stop_interval_detect: self.filtered_result_recorder.last ?? 1.0){
                    (result) -> () in
                    // The following code will be executed when the model is finished
                self.sc_filter.postprocess_interface(result, self.filtered_result_recorder.last ?? 1.0)
//                    self.raw_step_counter_output.append(result)
                    self.step_semaphore.signal()
                }
            self.step_semaphore.wait()
        }
    }
    
    func update_chart(){
            // Update charts in main thread
            DispatchQueue.main.async {
                self.chart_manager.updateChart(
                          Array(self.sc_filter.threshold_step_counter_res).map{Double($0)}.suffix(self.view_length),
                          self.sc_filter.raw_step_counter_res.suffix(self.view_length),
                          Array(self.sc_filter.fin_step_counter_res).map{Double($0)}.suffix(self.view_length),
                          self.chartView)
                
                self.counter_label.text = "Step Counter: \(self.sc_filter.threshold_step_counter_res.count)"
            }
        }
}
