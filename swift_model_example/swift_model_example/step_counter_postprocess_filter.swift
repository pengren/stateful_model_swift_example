//
//  step_counter_postprocess_filter.swift
//  CoreMotionExample
//
//  Created by renpeng on 6/1/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import AVFoundation
public class step_counter_postprocess_filter{
    var in_progress: Bool
    var raw_step_counter_res: [Double]
    var threshold_step_counter_res: [Int]
    var fin_step_counter_res: [Int]
    let threshold_in: Double
    let threshold_out: Double
    var in_interval: Bool
    
    var start_pos: Int
    var end_pos: Int
    
    var counter: Int
    
    init(_ threshold_in: Double, _ threshold_out: Double) {
        self.in_progress = false
        self.raw_step_counter_res = [Double]()
        self.threshold_step_counter_res = [Int]()
        self.fin_step_counter_res = [Int]()
        self.threshold_in = threshold_in
        self.threshold_out = threshold_out
        self.in_interval = false
        
        self.start_pos = 0
        self.end_pos = 0
        
        self.counter = 0 // Count the total step numbers
    }
    
    public func postprocess_interface(_ input_data: Double, _ Stop_interval_detect: Double) -> Int{
        self.raw_step_counter_res.append(input_data)
        
        let decision = self.decision_threshold_making(input_data)
        self.threshold_step_counter_res.append(decision)
        
        // Fill the fin_step_counter with zeros, will change it later
        self.fin_step_counter_res.append(0)
        
        return self.postprocess_filter(decision, Stop_interval_detect)
    }
    
    private func decision_threshold_making(_ input_data: Double) -> Int{
        if in_interval{
            if input_data < threshold_out{
                self.in_interval = false
                return 0
            }
            else{
                self.in_interval = true
                return 1
            }
        }
        else{
            if input_data > threshold_in{
                self.in_interval = true
                return 1
            }
            else{
                self.in_interval = false
                return 0
            }
        }
    }
    
    private func postprocess_filter(_ input_data: Int, _ Stop_interval_detect: Double) -> Int{
        if input_data == 1 && in_progress == false{
            in_progress = true
            // Begin the process
            self.start_pos = self.fin_step_counter_res.count
            //  print("S: \(self.start_pos), counter: \(self.threshold_step_counter_res.count)")
            // Don't know if this is necessary..
            self.end_pos = self.start_pos
            return 1
        }
        if input_data == 1 && in_progress == true{
            return 0
        }
        if input_data == 0 && in_progress == true{
            in_progress = false
            // End the process
            self.end_pos = self.fin_step_counter_res.count
            
            // Set the middle point to 1
            let middle_pos = Int((self.start_pos + self.end_pos - 2)/2)
//            print("\(self.threshold_step_counter_res.count)")
//            print("S: \(self.start_pos), E: \(self.end_pos), M: \(middle_pos)")
//            if Stop_interval_detect == 0.0{
//                self.fin_step_counter_res[middle_pos] = 1
//                self.counter += 1
//                AudioServicesPlaySystemSound(1113)
//            }
//            else{
//                AudioServicesPlaySystemSound(1104)
//            }
            
            self.fin_step_counter_res[middle_pos] = 1
            self.counter += 1
            AudioServicesPlaySystemSound(1113)
            
            // Don't know if this is necessary..
            self.start_pos = self.end_pos
            
//            print("counter: \(self.counter)")
            return 0
        }
        if input_data == 0 && in_progress == false{
            return 0
        }
        // the model should never return 0
        return -1
    }
}
