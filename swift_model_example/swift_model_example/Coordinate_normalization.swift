//
//  Coordinate_normalization.swift
//  CoreMotionExample
//
//  Created by renpeng on 6/15/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import CoreMotion

class Coordinate_normalization{
    
    // Normalize the coordinate & prepare the data to the RONIN model
    init() {
        
    }
    
    func process_data(rotationRate: CMRotationRate, userAcceRate: CMAcceleration, rotationMatrix: CMRotationMatrix) -> [Double]{
        // extract different kinds of data here
        let rot_x = rotationRate.x
        let rot_y = rotationRate.y
        let rot_z = rotationRate.z
        
        let usr_x = userAcceRate.x
        let usr_y = userAcceRate.y
        let usr_z = userAcceRate.z
        
        // Normalize the data above
        let rot_normalized = self.corrdinate_norm(
            rotationMatrix: rotationMatrix,
            x: rot_x,
            y: rot_y,
            z: rot_z)
        
        let usr_normalized = self.corrdinate_norm(
            rotationMatrix: rotationMatrix,
            x: usr_x,
            y: usr_y,
            z: usr_z)
        
        let combined_normalized_data = rot_normalized + usr_normalized
        assert(combined_normalized_data.count == 6)
        return combined_normalized_data
    }
    
    func corrdinate_norm(rotationMatrix: CMRotationMatrix, x: Double, y: Double, z: Double)->[Double]{
        // Use rotationMatrix to normalize the coordinate here
        let row1 = rotationMatrix.m11 * x + rotationMatrix.m21 * y + rotationMatrix.m31 * z
        let row2 = rotationMatrix.m12 * x + rotationMatrix.m22 * y + rotationMatrix.m32 * z
        let row3 = rotationMatrix.m13 * x + rotationMatrix.m23 * y + rotationMatrix.m33 * z
        return [row1, row2, row3]
    }
}
